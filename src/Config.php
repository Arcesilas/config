<?php

namespace Arcesilas\Config;

use Arcesilas\Config\Loader\LoaderException;
use Arcesilas\Config\Loader\LoaderInterface;
use Arcesilas\DotArray\DotArray;

class Config implements ConfigInterface
{
    /**
     * The configuration source
     * @var DotArray
     */
    protected $config;

    /**
     * The configuration loader
     * @var LoaderInterface
     */
    protected $loader;

    /**
     * The constructor.
     * @param  LoaderInterface $loader
     */
    public function __construct(LoaderInterface $loader)
    {
        $this->loader = $loader;
        $this->config = new DotArray();
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key, $default = null)
    {
        return $this->config->get($key, $default);
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $key, $value)
    {
        $this->config->set($key, $value);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function unset(string $key)
    {
        $this->config->unset($key);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function load(string $path)
    {
        try {
            $this->config->import(
                $this->loader->read($path)
            );
        } catch (LoaderException $e) {
            //
        }
        return $this;
    }
}
