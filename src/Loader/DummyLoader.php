<?php

namespace Arcesilas\Config\Loader;

class DummyLoader implements LoaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getExtensions(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function read(string $path): array
    {
        return [];
    }
}
