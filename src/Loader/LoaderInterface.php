<?php

namespace Arcesilas\Config\Loader;

interface LoaderInterface
{
    /**
     * Read a configuration file
     * @param  string $path
     * @return array
     */
    public function read(string $path): array;

    /**
     * Returns extensions handled by the loader
     * @return string[]
     */
    public function getExtensions(): array;
}
