<?php

namespace Arcesilas\Config\Loader;

class PhpLoader implements LoaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getExtensions(): array
    {
        return ['php'];
    }

    /**
     * {@inheritdoc}
     */
    public function read(string $path): array
    {
        if (file_exists($path) && !is_dir($path)) {
            $config = require $path;
            if (is_array($config)) {
                return $config;
            }
        }

        throw new LoaderException('Invalid configuration file: '.$path);
    }
}
