<?php

namespace Arcesilas\Config\Loader;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

class YamlLoader implements LoaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getExtensions(): array
    {
        return ['yml', 'yaml'];
    }

    /**
     * {@inheritdoc}
     */
    public function read(string $path): array
    {
        try {
            $return = Yaml::parseFile($path);
            if (is_array($return)) {
                return $return;
            }
        } catch (ParseException $e) {
            throw new LoaderException($e->getMessage());
        }

        throw new LoaderException('Invalid configuration file: '.$path);
    }
}
