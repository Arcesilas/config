<?php

namespace Arcesilas\Config\Loader;

class IniLoader implements LoaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getExtensions(): array
    {
        return ['ini'];
    }

    /**
     * {@inheritdoc}
     */
    public function read(string $path): array
    {
        if (file_exists($path) && !is_dir($path)) {
            $config = @parse_ini_file($path, true, INI_SCANNER_TYPED);
            if (false !== $config) {
                return $config;
            }
        }

        throw new LoaderException('Invalid configuration file: '.$path);
    }
}
