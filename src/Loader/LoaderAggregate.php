<?php

namespace Arcesilas\Config\Loader;

use Arcesilas\DotArray\DotArray;

class LoaderAggregate implements LoaderInterface
{
    /**
     * The loaders aggregated
     * @var LoaderInterface[]
     */
    protected $loaders = [];

    /**
     * The paths where to search configuration
     * @var string[]
     */
    protected $rootPaths = [];

    /**
     * {@inheritdoc}
     */
    public function read(string $path): array
    {
        $path = $this->resolvePath($path);
        $loader = $this->getAppropriateLoader($path);
        return $loader->read($path);
    }

    /**
     * {@inheritdoc}
     */
    public function getExtensions(): array
    {
        return array_keys($this->loaders);
    }

    /**
     * Add a loader
     * @param LoaderInterface $loader
     */
    public function add(LoaderInterface $loader)
    {
        foreach ($loader->getExtensions() as $extension) {
            $this->loaders[$extension] = $loader;
        }
        ksort($this->loaders);
    }

    /**
     * Add a rootPath
     * @param  string  $root
     */
    public function addRoot(string $root)
    {
        if (! in_array($root, $this->rootPaths)) {
            $this->rootPaths[] = $root;
        }
    }

    /**
     * Resolves path using handled extensions and rootPaths
     * @param  string      $path
     * @return string
     */
    protected function resolvePath(string $path): string
    {
        // The full path was provided
        if (file_exists($path)) {
            return $path;
        }

        // Only filename was provided, with or without the extensions
        foreach ($this->rootPaths as $p) {
            $fullPath = rtrim($p, '/').'/'.ltrim($path, '/');
            if (file_exists($fullPath)) {
                return $fullPath;
            }
            if (null !== ($fullPath = $this->resolvePathExtension($fullPath))) {
                return $fullPath;
            }
        }

        // Finally try considering the full path was provided without the file extension
        return $this->resolvePathExtension($path) ?? $path;
    }

    /**
     * Try to resolve path using handled extensions
     * @param  string $path
     * @return string|null
     */
    protected function resolvePathExtension(string $path): ?string
    {
        foreach ($this->loaders as $ext => $loader) {
            if (file_exists($path.'.'.$ext)) {
                return $path.'.'.$ext;
            }
        }
        return null;
    }

    /**
     * Returns the appropriate loader for a given file
     * @param  string          $path
     * @return LoaderInterface
     */
    protected function getAppropriateLoader(string $path): LoaderInterface
    {
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        if (array_key_exists($extension, $this->loaders)) {
            return $this->loaders[$extension];
        }

        return new DummyLoader();
    }
}
