<?php

namespace Arcesilas\Config;

interface ConfigInterface
{
    /**
     * Returns a configuration value
     * @param  string $key
     * @param  mixed $default
     * @return mixed
     */
    public function get(string $key, $default = null);

    /**
     * Set a configuration value
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, $value);

    /**
     * Unset a configuration value
     * @param string $key
     */
    public function unset(string $key);

    /**
     * Load a configuration file
     * @param  string $path
     */
    public function load(string $path);
}
