<?php

namespace Arcesilas\Config;

use Arcesilas\Config\Config;
use Arcesilas\Config\ConfigInterface;
use Arcesilas\Config\Loader\LoaderAggregate;
use Arcesilas\Config\Loader\LoaderException;
use Arcesilas\Config\Loader\LoaderInterface;
use Arcesilas\DotArray\DotArray;

class ConfigManager extends Config implements ConfigInterface
{
    /**
     * LoaderAggregate
     * @var LoaderAggregate
     */
    protected $loader;

    protected $namespaces = [];

    public function __construct()
    {
        $this->loader = new LoaderAggregate();
        $this->config = new DotArray();
    }

    /**
     * {@inheritdoc}
     */
    public function load(string $path)
    {
        $key = pathinfo($path, PATHINFO_FILENAME);
        $this->loadInto($path, $key);
        return $this;
    }

    /**
     * Load a configuration into a given namespace
     * @param  string $path
     * @param  string $namespace
     */
    public function loadInto(string $path, string $namespace)
    {
        try {
            $this->config->import(
                [$namespace => $this->loader->read($path)]
            );
            $this->namespaces[$namespace] = true;
        } catch (LoaderException $e) {
            $this->namespaces[$namespace] = false;
        }
        return $this;
    }

    /**
     * Add a loader. Shortcut for loaderAggregate->load()
     * @param LoaderInterface $loader
     */
    public function addLoader(LoaderInterface $loader)
    {
        $this->loader->add($loader);
        return $this;
    }

    /**
     * Add a rootPath. Shortcut for loaderAggregate->addRoot()
     * @param string $root
     */
    public function addRoot(string $root)
    {
        $this->loader->addRoot($root);
        $this->namespaces = [];
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * Try to autoload the appropriate file depending on the requested key
     */
    public function get(string $key, $default = null)
    {
        $namespace = substr($key, 0, strpos($key, '.'));

        if (!array_key_exists($namespace, $this->namespaces)) {
            $this->load($namespace);
        }

        return parent::get($key, $default);
    }
}
