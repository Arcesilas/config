[![Packagist](https://img.shields.io/packagist/v/arcesilas/config.svg?style=flat-square)]()
[![PHP from Packagist](https://img.shields.io/packagist/php-v/arcesilas/config.svg?style=flat-square)]()
[![license](https://img.shields.io/github/license/Arcesilas/config.svg?style=flat-square)]()
[![Scrutinizer](https://img.shields.io/scrutinizer/g/Arcesilas/config.svg?style=flat-square)]()
[![Scrutinizer Coverage](https://img.shields.io/scrutinizer/coverage/g/Arcesilas/config.svg?style=flat-square)]()
[![Scrutinizer Build](https://img.shields.io/scrutinizer/build/g/Arcesilas/config.svg?style=flat-square)]()

# Arcesilas/Config

This package helps to handle configuration files: Yaml, PHP and Ini are currently supported.

## Install

Using composer:

```shell
composer require arcesilas/config
```

## Quick start

If you only need to load one file, use the `Config` class:

```php
use Arcesilas\Config\Config;
use Arcesilas\Config\Loader\YamlLoader;

$config = new Config(new YamlLoader());
$config->load('/path/to/config.yaml');
```

Let's say your `config.yaml` file looks like this:

```yaml
foo: 42
bar:
    baz: 1337
```

You can access configuration keys like this:

```php
$config->get('foo'); // 42
$config->get('bar'); // ['baz => 1337']
$config->get('bar.baz'); // 1337
```

You may specify a default value, as second argument:

```php
$config->get('undefined', 3.14159); // 3.14159
```

You can also specify a Closure as default value, it will be executed:

```php
$config->get('undefined', function () {
    return 6 * 7;
});
// Returns 42
```

## Configuration Manager

The configuration manager allows you to handle more that one configuration file, several loader (Yaml, Php, Ini for example) and autoload files from several paths.

The `ConfigManager` and `Config` classes both implement the same `ConfigInterface` interface, so, you can use them the same way. The `ConfigManager` class has some additional methods to allow you handle several paths, loaders and configuration files.

### Add loaders

```php
$config = new ConfigManager();
$config->addLoader(new YamlLoader());
$config->addLoader(new PhpLoader());

// Now, you can load both Yaml and PHP configuration files
```

### Add a root path

```php
$config = new ConfigManager();
$config->addRoot('/path/to/config/');
```

From now, when loading a file, you don't have to give its full path anymore, only its path relative to the root you've defined:

```php
$config->load('app.yaml');
```

The file `/path/to/config/app.yaml` will be loaded (if you have added the YamlLoader).

### Loading files

The main difference between `Config` and `ConfigManager` is the configuration namespacing.

The `Config` class uses only one file, so it's just loaded. Period.

The `ConfigManager` can load multiple configuration files. Their filenames are used to namespace them.

Example:

`app.yaml`:
```yaml
timezone: UTC
locale: en_GB
homepage: index
```

```php
$config = new ConfigManager();
$config->addLoader(new YamlLoader());
$config->load('/path/to/app.yaml');
```

The configurations items in `app.yaml` will be accessible using `app` as namespace, ie as a key prefix:
```php
$timezone = $config->get('app.timezone'); // UTC
```

With `dev.yaml`:
```yaml
timezone: America/New_York
```

```php
$config->load('/path/to/dev.yaml');
$config->get('dev.timezone'); // America/New_York
$config->get('app.timezone'); // UTC
```

If you want to load a file in a specific namespace (for instance, if you want to override a default configuation with user's configuration), use the `loadInto()` method:

`custom.yaml`:
```yaml
timezone: Europe/Paris
locale: fr_FR
```

```php
$config->loadInto('path/to/custom.yaml', 'app');
$config->get('app.timezone'); // Europe/Paris
$config->get('app.homepage'); // index
```

### Files autoload

When using `ConfigManager`, you don't need to load the file beforehand, if it's located in one of the defined roots.

`something.yaml`:
```yaml
foo: bar
baz: 42
```

```php
$config->addRoot('/path/to/config/dir');
$config->get('something.baz'); // 42
```
