<?php

namespace Arcesilas\Config\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Arcesilas\Config\ConfigManager;
use Arcesilas\Config\Loader\LoaderAggregate;
use Arcesilas\Config\Loader\LoaderException;
use Arcesilas\Config\Loader\LoaderInterface;
use Arcesilas\DotArray\DotArray;

class ConfigManagerTest extends TestCase
{
    protected $config;

    protected $loader;

    protected $dotArray;

    protected $ref;

    protected $refLoaderAggregate;

    public function setUp()
    {
        $this->loader = $this->createMock(LoaderAggregate::class);
        $this->config = new ConfigManager();

        $this->dotArray = $this->createMock(DotArray::class);

        $this->ref = new \ReflectionObject($this->config);
        $refDotArray = $this->ref->getProperty('config');
        $refDotArray->setAccessible(true);
        $refDotArray->setValue($this->config, $this->dotArray);

        $this->refLoaderAggregate = $this->ref->getProperty('loader');
        $this->refLoaderAggregate->setAccessible(true);
        $this->refLoaderAggregate->setValue($this->config, $this->loader);
    }

    public function testLoadInto()
    {
        $this->loader->expects($this->once())
            ->method('read')
            ->with($this->equalTo('/path/to/bar.ext'))
            ->willReturn(['foo' => 42]);

        $this->dotArray->expects($this->once())
            ->method('import')
            ->with($this->equalTo(['something' => ['foo' => 42]]));

        $this->config->loadInto('/path/to/bar.ext', 'something');
    }

    public function testLoad()
    {
        $this->loader->expects($this->once())
            ->method('read')
            ->with($this->equalTo('/path/to/bar.ext'))
            ->willReturn(['foo' => 42]);

        $this->dotArray->expects($this->once())
            ->method('import')
            ->with($this->equalTo(['bar' => ['foo' => 42]]));

        $this->config->load('/path/to/bar.ext');
    }

    public function testLoadsNothingOnError()
    {
        $this->loader->expects($this->once())
            ->method('read')
            ->with($this->equalTo('/path/to/bar.ext'))
            ->will($this->throwException(new LoaderException));

        $this->config->load('/path/to/bar.ext');
    }

    public function testAddLoader()
    {
        $loader = $this->createMock(LoaderInterface::class);

        $loaderAggregate = $this->refLoaderAggregate->getValue($this->config);

        $loaderAggregate->expects($this->once())
            ->method('add')
            ->with($loader);

        $this->config->addLoader($loader);
    }

    public function testAddRoot()
    {
        $this->loader->expects($this->once())
            ->method('addRoot')
            ->with('/path/to/config');

        $this->config->addRoot('/path/to/config');
    }
}
