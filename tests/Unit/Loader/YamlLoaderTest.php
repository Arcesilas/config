<?php

namespace Arcesilas\Config\Tests\Unit\Loader;

use Arcesilas\Config\Tests\Assets\AbstractLoaderTestCase;

class YamlLoaderTest extends AbstractLoaderTestCase
{
    protected $loader = 'YamlLoader';

    public function extensionsProvider()
    {
        return [
            ['yaml'],
            ['yml']
        ];
    }
}
