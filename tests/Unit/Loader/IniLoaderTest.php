<?php

namespace Arcesilas\Config\Tests\Unit\Loader;

use Arcesilas\Config\Tests\Assets\AbstractLoaderTestCase;

class IniLoaderTest extends AbstractLoaderTestCase
{

    protected $loader = 'IniLoader';

    public function extensionsProvider()
    {
        return [
            ['ini']
        ];
    }

}
