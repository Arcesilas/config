<?php

namespace Arcesilas\Config\Tests\Unit\Loader;

use PHPUnit\Framework\TestCase;
use Arcesilas\Config\Loader\DummyLoader;

class DummyLoaderTest extends TestCase
{

    protected $root = __DIR__.'/../Assets/files';


    protected function setUp()
    {
        $this->loader = new DummyLoader();
    }

    public function testGetExtensions()
    {
        $this->assertEquals([], $this->loader->getExtensions());
    }

    public function testReadValidFile()
    {
        $this->assertEquals([], $this->loader->read(__DIR__.'/../Assets/root1/valid.yaml'));
    }

    public function testReadNonexistantFile()
    {
        $this->assertEquals([], $this->loader->read(__DIR__.'/paht/to/nowhere'));
    }
}
