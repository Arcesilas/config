<?php

namespace Arcesilas\Config\Tests\Unit\Loader;

use PHPUnit\Framework\TestCase;
use Arcesilas\Config\Loader\DummyLoader;
use Arcesilas\Config\Loader\PhpLoader;
use Arcesilas\Config\Loader\YamlLoader;
use Arcesilas\Config\Loader\LoaderAggregate;
use Arcesilas\Config\Loader\LoaderInterface;

class LoaderAggregateTest extends TestCase
{
    protected $loader;

    public function setUp()
    {
        $this->loader = new LoaderAggregate();
    }

    public function testAddLoader()
    {
        $this->loader->add(new YamlLoader());
        $extensions = array_keys($this->readAttribute($this->loader, 'loaders'));

        $this->assertEquals(['yaml', 'yml'], $extensions);

        $this->loader->add(new PhpLoader());

        $extensions = array_keys($this->readAttribute($this->loader, 'loaders'));
        $this->assertEquals(['php', 'yaml', 'yml'], $extensions);
    }

    public function testAddRoot()
    {
        $this->loader->addRoot(__DIR__);
        $this->assertAttributeEquals([__DIR__], 'rootPaths', $this->loader);
        // Should not be added again if already present
        $this->loader->addRoot(__DIR__);
        $this->assertAttributeEquals([__DIR__], 'rootPaths', $this->loader);
    }

    public function resolvePathsProvider()
    {
        return [
            [__DIR__.'/undefined/', 'foo.yaml', 'foo.yaml'],
            [__DIR__.'/../../Assets/root1/', 'foo.yaml', __DIR__.'/../../Assets/root1/foo.yaml'],
            [__DIR__.'/../../Assets/root2/', 'foo.yaml', 'foo.yaml'],
            [__DIR__.'/../../Assets/root2/', __DIR__.'/../../Assets/root1/foo.yaml', __DIR__.'/../../Assets/root1/foo.yaml']
        ];
    }

    /**
     * @dataProvider resolvePathsProvider
     */
    public function testResolvePath(string $rootPath, $file, $expected)
    {
        $ref = new \ReflectionObject($this->loader);
        $refRootPaths = $ref->getProperty('rootPaths');
        $refRootPaths->setAccessible(true);
        $refRootPaths->setvalue($this->loader, [$rootPath]);

        $refResolvePath = $ref->getMethod('resolvePath');
        $refResolvePath->setAccessible(true);

        $resolved = $refResolvePath->invoke($this->loader, $file);

        $this->assertEquals($expected, $resolved);
    }

    public function testRead()
    {
        $loader = $this->createMock(LoaderInterface::class);
        $loader->method('read')
            ->willReturn(['foo' => 42]);

        $loaderAggregate = $this->getMockBuilder(LoaderAggregate::class)
            ->setMethods(['resolvePath', 'getAppropriateLoader'])
            ->getMock();

        $loaderAggregate->expects($this->once())
            ->method('resolvePath')
            ->with($this->equalTo('somefile.ext'))
            ->willReturn('somefile.ext');

        $loaderAggregate->expects($this->once())
            ->method('getAppropriateLoader')
            ->with($this->equalTo('somefile.ext'))
            ->willReturn($loader);

        $ref = new \ReflectionObject($loaderAggregate);
        $refLoaders = $ref->getProperty('loaders');
        $refLoaders->setAccessible(true);

        $refLoaders->setValue($loaderAggregate, ['ext' => $loader]);

        $this->assertEquals(['foo' => 42], $loaderAggregate->read('somefile.ext'));
    }

    protected function setFakeLoaders($loaders)
    {
        $ref = new \ReflectionObject($this->loader);
        $refLoaders = $ref->getProperty('loaders');
        $refLoaders->setAccessible(true);

        $refLoaders->setValue($this->loader, $loaders);
    }

    protected function getFakeLoaders()
    {
        $loader1 = $this->createMock(LoaderInterface::class);
        $loader2 = $this->createMock(LoaderInterface::class);
        return [
            'foo' => $loader1,
            'bar' => $loader2,
            'baz' => $loader1,
        ];
    }

    public function testGetExtensions()
    {
        $this->setFakeLoaders($this->getFakeLoaders());
        $actual = $this->loader->getExtensions();
        sort($actual);
        $this->assertEquals(['bar', 'baz', 'foo'], $actual);
    }

    public function testGetAppropriateLoader()
    {
        $loaders = $this->getFakeLoaders();
        $this->setFakeLoaders($loaders);

        $ref = new \ReflectionObject($this->loader);
        $refMethod = $ref->getMethod('getAppropriateLoader');
        $refMethod->setAccessible(true);

        foreach ($loaders as $extension => $loader) {
            $this->assertSame($loader, $refMethod->invoke($this->loader, 'file.'.$extension));
        }

        $this->assertInstanceOf(DummyLoader::class, $refMethod->invoke($this->loader, 'file.ext'));
    }
}
