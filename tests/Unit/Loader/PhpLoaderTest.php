<?php

namespace Arcesilas\Config\Tests\Unit\Loader;

use Arcesilas\Config\Tests\Assets\AbstractLoaderTestCase;

class PhpLoaderTest extends AbstractLoaderTestCase
{
    protected $loader = 'PhpLoader';

    public function extensionsProvider()
    {
        return [
            ['php']
        ];
    }
}
