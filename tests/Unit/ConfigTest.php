<?php

namespace Arcesilas\Config\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Arcesilas\Config\Config;
use Arcesilas\Config\Loader\LoaderException;
use Arcesilas\Config\Loader\LoaderInterface;
use Arcesilas\DotArray\DotArray;

class ConfigTest extends TestCase
{
    protected $config;

    protected $loader;

    protected $dotArray;

    public function setUp()
    {
        $this->loader = $this->createMock(LoaderInterface::class);
        $this->config = new Config($this->loader);

        $this->dotArray = $this->createMock(DotArray::class);

        $ref = new \ReflectionObject($this->config);
        $refDotArray = $ref->getProperty('config');
        $refDotArray->setAccessible(true);
        $refDotArray->setValue($this->config, $this->dotArray);
    }

    public function testGet()
    {
        $this->dotArray->expects($this->once())
            ->method('get')
            ->with($this->equalTo('foo'))
            ->willReturn(42);

        $this->assertEquals(42, $this->config->get('foo'));
    }

    public function testSet()
    {
        $this->dotArray->expects($this->once())
            ->method('set')
            ->with(
                $this->equalTo('foo'),
                $this->equalTo(42)
            );
        $this->config->set('foo', 42);
    }

    public function testUnset()
    {
        $this->dotArray->expects($this->once())
            ->method('unset')
            ->with($this->equalTo('foo'));
        $this->config->unset('foo');
    }

    public function testLoad()
    {
        $this->loader->expects($this->once())
            ->method('read')
            ->with($this->equalTo('/path/to/config.ext'))
            ->willReturn(['foo' => 42]);

        $this->dotArray->expects($this->once())
            ->method('import')
            ->with($this->equalTo(['foo' => 42]));

        $this->config->load('/path/to/config.ext');
    }

    public function testLoadsNothingOnError()
    {
        $this->loader->expects($this->once())
            ->method('read')
            ->with($this->equalTo('/path/to/config.ext'))
            ->will($this->throwException(new LoaderException()));

        $this->config->load('/path/to/config.ext');
    }
}
