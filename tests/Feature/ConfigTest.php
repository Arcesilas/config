<?php

namespace Arcesilas\Config\Tests\Feature;

use PHPUnit\Framework\TestCase;
use Arcesilas\Config\Config;
use Arcesilas\Config\Loader;

class ConfigTest extends TestCase
{
    protected $root;

    public function setUp()
    {
        $this->root = __DIR__.'/../Assets/files/';
    }

    public function filesProvider()
    {
        return [
            [Loader\YamlLoader::class, 'config.yaml', 'config2.yml'],
            [Loader\PhpLoader::class,  'config.php',  'config2.php'],
            [Loader\IniLoader::class,  'config.ini',  'config2.ini']
        ];
    }

    /**
     * @dataProvider filesProvider
     */
    public function testLoaders($loader, $configFile, $configFile2)
    {
        $config = new Config(new $loader());
        $config->load($this->root.$configFile);

        $this->assertSame(10, $config->get('posts_per_page'));
        $this->assertEquals('Europe/Paris', $config->get('l10n.timezone'));
        $this->assertEquals('fr_FR', $config->get('l10n.locale'));

        $config->load($this->root.$configFile2);

        $this->assertSame(5, $config->get('posts_per_page'));
        $this->assertEquals('UTC', $config->get('l10n.timezone'));
        $this->assertEquals('fr_FR', $config->get('l10n.locale'));

        $this->assertEquals('Example', $config->get('post.title'));

        $config->set('post.title', 'Lorem ipsum');
        $this->assertEquals('Lorem ipsum', $config->get('post.title'));

        $postData = $config->get('post');
        $this->assertEquals('Lorem ipsum', $postData['title']);
    }
}
