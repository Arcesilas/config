<?php

namespace Arcesilas\Config\Tests\Feature;

use PHPUnit\Framework\TestCase;
use Arcesilas\Config\ConfigManager;
use Arcesilas\Config\Loader;

class ConfigManagerTest extends TestCase
{
    public function testLoad()
    {
        $config = new ConfigManager();

        $this->assertNull($config->get('foo.answer'));
        $this->assertNull($config->get('bar.leet'));

        $config->addLoader(new Loader\YamlLoader());

        $config->addRoot(__DIR__.'/../Assets/root1');
        $config->load('foo');

        $this->assertEquals(42, $config->get('foo.answer'));
        $this->assertNull($config->get('bar.leet'));

        $config->addRoot(__DIR__.'/../Assets/root2');
        $config->load('bar');

        $this->assertEquals(42, $config->get('foo.answer'));
        $this->assertEquals(1337, $config->get('bar.leet'));
    }

    public function testAutoload()
    {
        $config = new ConfigManager();

        $this->assertNull($config->get('foo.answer'));

        $config->addLoader(new Loader\YamlLoader());
        $config->addRoot(__DIR__.'/../Assets/root1');

        $this->assertEquals(42, $config->get('foo.answer'));
    }
}
