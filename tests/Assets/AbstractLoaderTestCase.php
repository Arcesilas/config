<?php

namespace Arcesilas\Config\Tests\Assets;

use PHPUnit\Framework\TestCase;
use Arcesilas\Config\Loader\LoaderException;

class AbstractLoaderTestCase extends TestCase
{
    protected $root = __DIR__.'/files';

    protected function setUp()
    {
        $loader = 'Arcesilas\\Config\\Loader\\' . $this->loader;
        $this->loader = new $loader();
    }

    protected $expected = ['foo' => 42];

    public function testGetExtensions()
    {
        $expected = [];
        foreach ($this->extensionsProvider() as $array) {
            if (isset($array[0])) {
                $expected[] = $array[0];
            }
        }

        $extensions = $this->loader->getExtensions();

        sort($expected);
        sort($extensions);

        $this->assertEquals($extensions, $extensions);
    }

    /**
     * @dataProvider extensionsProvider
     */
    public function testReadValidFile($extension)
    {
        $path = $this->root.'/valid.'.$extension;
        $this->assertEquals($this->expected, $this->loader->read($path));
    }

    /**
     * @dataProvider extensionsProvider
     */
    public function testReadInvalidFile($extension)
    {
        $path = $this->root.'/invalid.'.$extension;
        $this->expectException(LoaderException::class);
        $this->loader->read($path);
    }

    /**
     * @dataProvider extensionsProvider
     */
    public function testReadNonexistantFile($extension)
    {
        $path = $this->root.'/nonexistant.'.$extension;
        $this->expectException(LoaderException::class);
        $this->loader->read($path);
    }

}
