<?php

return [
    'posts_per_page' => 5,
    'l10n' => [
        'timezone' => 'UTC',
    ],
];
