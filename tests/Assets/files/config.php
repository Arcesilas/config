<?php

return [
    'posts_per_page' => 10,
    'l10n' => [
        'timezone' => 'Europe/Paris',
        'locale' => 'fr_FR'
    ],
    'post' => [
        'title' => 'Example',
        'date' => '2018-02-10'
    ],
];
